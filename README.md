# README #

Bamboo plugin which allows to mark builds as Important or Urgent. Urgent builds will be added to the top of build queue.

Compatible with Bamboo 5.10+

Download at [Marketplace](https://marketplace.atlassian.com/plugins/com.atlassianlab.bamboo.plugins.bamboo-queue-priority-plugin/server/overview).

###How to build ###
Requires 

* Maven 3.2+ with [Atlassian Maven repository](https://developer.atlassian.com/docs/advanced-topics/working-with-maven/atlassian-maven-repositories) added to your Maven settings.xml 
* JDK 1.8
* [Atlassian SDK 6.2.15](https://developer.atlassian.com/docs/getting-started/downloads)

Checkout
```
mvn clean package
```
Install plugin from target folder via UPM

## Documentation ##

System administrator can configure access to build priority configuration only for admin users, go to <BAMBOO_URL>/admin/plugins/bamboo-queue-priority-plugin/configurePlugin.action

Legal information
=================

To contribute if you are an Atlassian customer no further action is required because our
[End User Agreement](http://www.atlassian.com/end-user-agreement/) (Section 7) gives Atlassian the right to use
contributions from customers.
If your are not an Atlassian customer then you will need to sign and submit our [Contribution Agreement](ACLA.pdf).


How to contribute
=================
To contribute you code to the plugin please:
* create a feature branch,
* change the code,
* add unit and/or integration tests,
* test it and
* eventually create a pull request to review them.
After our review, your feature branch will be merged into the master branch!

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

See _Contributors License Agreement_ section of [Open Source at Atlassian](https://developer.atlassian.com/opensource/) page.


### Who do I talk to? ###

* Alexey Chystoprudov, achystoprudov at atlassian.com

### License ###

```
Copyright (c) 2016, Atlassian Pty. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
The names of contributors may not
be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```