package it.com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.testutils.TestBuildDetailsBuilder;
import com.atlassian.bamboo.testutils.UniqueNameHelper;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.atlassian.bamboo.webdriver.TestInjectionTestRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.collect.ImmutableList;
import it.com.atlassianlab.bamboo.plugins.queuepriority.page.MiscellaneousConfigurationPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class QueuePriorityTest
{
    private final WebDriverTestEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    private final BambooTestedProduct bamboo = BambooTestedProductFactory.create();

    @Rule public final BackdoorRule backdoor = new BackdoorRule(environmentData);
    @Rule public final TestInjectionTestRule injectionRule = new TestInjectionTestRule(this, bamboo);

    @Inject private Timeouts timeouts;

    @Before
    public void setUp()
    {
        backdoor.agents().disableAllAgents();
    }

    @Test
    public void buildQueueAffectedBySettings() throws Exception
    {
        //create four plans with different priorities and trigger them
        final TestBuildDetails idle = new TestBuildDetailsBuilder()
                .withScriptTask("echo idle")
                .withNoInitialBuild()
                .withName(UniqueNameHelper.makeUniqueName("idle"))
                .build();
        final TestBuildDetails defaultPriority = new TestBuildDetailsBuilder()
                .withScriptTask("echo default")
                .withNoInitialBuild()
                .withName(UniqueNameHelper.makeUniqueName("default"))
                .build();
        final TestBuildDetails important = new TestBuildDetailsBuilder()
                .withScriptTask("echo important")
                .withNoInitialBuild()
                .withName(UniqueNameHelper.makeUniqueName("important"))
                .build();
        final TestBuildDetails urgent = new TestBuildDetailsBuilder()
                .withScriptTask("echo urgent")
                .withNoInitialBuild()
                .withName(UniqueNameHelper.makeUniqueName("urgent"))
                .build();
        backdoor.plans().createPlan(idle);
        setPriority(idle, "IDLE");
        backdoor.plans().createPlan(defaultPriority);
        backdoor.plans().createPlan(important);
        setPriority(important, "IMPORTANT");
        backdoor.plans().createPlan(urgent);
        setPriority(urgent, "URGENT");
        backdoor.plans().triggerBuild(idle.getKey());
        Poller.waitUntil(backdoor.queue().getResultKeysInQueue(timeouts), hasSize(1));
        backdoor.plans().triggerBuild(defaultPriority.getKey());
        Poller.waitUntil(backdoor.queue().getResultKeysInQueue(timeouts), hasSize(2));
        backdoor.plans().triggerBuild(important.getKey());
        Poller.waitUntil(backdoor.queue().getResultKeysInQueue(timeouts), hasSize(3));
        backdoor.plans().triggerBuild(urgent.getKey());

        //confirm queue order is priority-aware

        final List<PlanResultKey> queueJobs = Poller.waitUntil(backdoor.queue().getResultKeysInQueue(timeouts), hasSize(4));
        final ImmutableList<TestBuildDetails> plans = ImmutableList.of(urgent, important, defaultPriority, idle);
        assertThat("Urgent build should be first in queue. " + debugMessage(plans, queueJobs), queueJobs.get(0).getPlanKey().getKey(), containsString(urgent.getKey().getKey()));
        assertThat("Important build should be second in queue. " + debugMessage(plans, queueJobs), queueJobs.get(1).getPlanKey().getKey(), containsString(important.getKey().getKey()));
        assertThat("Default build should be third in queue. " + debugMessage(plans, queueJobs), queueJobs.get(2).getPlanKey().getKey(), containsString(defaultPriority.getKey().getKey()));
        assertThat("Idle build should be last in queue. " + debugMessage(plans, queueJobs), queueJobs.get(3).getPlanKey().getKey(), containsString(idle.getKey().getKey()));
    }

    private String debugMessage(List<TestBuildDetails> plans, List<PlanResultKey> queueJobs)
    {
        return "Expected order: " + plans.stream().map(plan -> plan.getKey().getKey()).collect(Collectors.joining(",")) + ". Actual order: " + queueJobs.stream().map(jobResult -> jobResult.getPlanKey().getKey()).collect(Collectors.joining(","));
    }

    private void setPriority(TestBuildDetails plan, String priorityLabel)
    {
        bamboo.fastLogin(TestUser.ADMIN);
        MiscellaneousConfigurationPage page = bamboo.visit(MiscellaneousConfigurationPage.class, plan);
        page.setPriority(priorityLabel);
        page.save();
    }

    @After
    public void tearDown()
    {
        backdoor.agents().enableAllAgents();
    }
}