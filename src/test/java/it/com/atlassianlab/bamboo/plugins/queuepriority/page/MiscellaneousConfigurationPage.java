package it.com.atlassianlab.bamboo.plugins.queuepriority.page;

import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;

public class MiscellaneousConfigurationPage extends com.atlassian.bamboo.pageobjects.pages.plan.configuration.MiscellaneousConfigurationPage
{
    @ElementBy(name="custom.queue.priority")
    private SelectElement priority;

    public MiscellaneousConfigurationPage(TestBuildDetails plan)
    {
        super(plan);
    }

    public void setPriority(String priorityLabel)
    {
        priority.select(Options.value(priorityLabel));
    }
}
