package com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.plan.cache.ImmutableChain;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import static com.atlassianlab.bamboo.plugins.queuepriority.PlanPriorityConfigurationPlugin.BUILD_PRIORITY_KEY;

public enum Priority
{
    URGENT
            {
                @Override
                public boolean pass(Priority priority)
                {
                    return false;
                }
            },
    IMPORTANT
            {
                @Override
                public boolean pass(Priority priority)
                {
                    return priority == URGENT;
                }
            },
    DEFAULT
            {
                @Override
                public boolean pass(Priority priority)
                {
                    return priority != DEFAULT && priority != IDLE;
                }
            },
    IDLE
            {
                @Override
                public boolean pass(Priority priority)
                {
                    return priority != IDLE;
                }
            };

    public static Priority fromValue(@Nullable Object value)
    {
        if (value == null || StringUtils.isEmpty(value.toString()))
        {
            return DEFAULT;
        }
        try
        {
            return Priority.valueOf(value.toString());
        }
        catch (IllegalArgumentException e)
        {
            return DEFAULT;
        }
    }

    /**
     * @return true if given priority has higher urgency.
     */
    public abstract boolean pass(Priority priority);

    public static Priority forChain(ImmutableChain chain)
    {
        return Priority.fromValue(chain.getBuildDefinition().getCustomConfiguration().get(BUILD_PRIORITY_KEY));
    }
}
