package com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.v2.build.events.BuildQueuedEvent;
import com.atlassian.event.api.EventListener;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class BuildQueuedListener
{
    private final CachedPlanManager planManager;
    private final QueueReorderer queueReorderer;
    private Logger log = Logger.getLogger(BuildQueuedListener.class);

    @Autowired
    public BuildQueuedListener(QueueReorderer queueReorderer, CachedPlanManager planManager)
    {
        this.queueReorderer = queueReorderer;
        this.planManager = planManager;
    }

    @EventListener
    public void onEvent(BuildQueuedEvent event)
    {
        final PlanResultKey queuedJobResultKey = event.getPlanResultKey();
        final ImmutableJob job = planManager.getPlanByKey(queuedJobResultKey.getPlanKey(), ImmutableJob.class);
        if (job == null)
        {
            log.info("Can't cast " + queuedJobResultKey + " to job");
            return;
        }
        final ImmutableChain queuedPlan = job.getParent();
        final Priority queuedPlanPriority = Priority.forChain(queuedPlan);
        if (queuedPlanPriority == Priority.IDLE)
        {
            log.debug("Plan " + queuedJobResultKey.getPlanKey() + " skips prioritization");
            return;
        }

        queueReorderer.changeOrder(queuedJobResultKey, queuedPlanPriority);
    }
}