package com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.v2.build.queue.BuildQueuePosition;
import com.atlassian.bamboo.v2.build.queue.QueueManagerView;
import com.atlassian.event.api.EventPublisher;
import com.atlassianlab.bamboo.plugins.queuepriority.event.GenericAnalyticsEvent;
import com.atlassianlab.bamboo.plugins.queuepriority.event.ReorderedEvent;
import com.google.common.base.Functions;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class QueueReorderer
{
    private static final Logger log = Logger.getLogger(QueueReorderer.class);
    private final BuildQueueManager buildQueueManager;
    private final CachedPlanManager planManager;
    private final EventPublisher eventPublisher;

    public QueueReorderer(final BuildQueueManager buildQueueManager,
                          final CachedPlanManager planManager,
                          final EventPublisher eventPublisher)
    {
        this.buildQueueManager = buildQueueManager;
        this.planManager = planManager;
        this.eventPublisher = eventPublisher;
    }

    void changeOrder(final PlanResultKey queuedPlanResultKey,
                     final Priority queuedPlanPriority)
    {
        final QueueManagerView<CommonContext, CommonContext> queueManagerView =
                QueueManagerView.newView(buildQueueManager, Functions.identity());
        final Iterable<BuildQueueManager.QueueItemView<CommonContext>> queueView = queueManagerView.getQueueView(Collections.emptyList());

        final List<PlanResultKey> queuedPlans = StreamSupport.stream(queueView.spliterator(), false)
                .map(itemView -> itemView.getView().getResultKey())
                // -> ResultKey
                .filter(key -> !key.equals(queuedPlanResultKey)) //skip current job
                .map(key -> Narrow.downTo(key, PlanResultKey.class))
                // -> PlanResultKey
                .filter(Objects::nonNull) //skip deployments
                .filter(key ->
                {
                    final ImmutableJob job = planManager.getPlanByKey(key.getPlanKey(), ImmutableJob.class);
                    if (job == null)
                    {
                        return false;
                    }
                    final ImmutableChain chain = job.getParent();
                    return !Priority.forChain(chain).pass(queuedPlanPriority);
                })
                .collect(Collectors.toList());
        final BuildQueuePosition position = buildQueueManager.getQueuePosition(queuedPlanResultKey);
        if (queuedPlans.isEmpty())
        {
            final boolean success = buildQueueManager.reorderInQueue(queuedPlanResultKey, 0);
            if (success)
            {
                eventPublisher.publish(new ReorderedEvent(0, position.getQueuePosition() + 1));
            }
            else
            {
                log.info("Couldn't reorder " + queuedPlanResultKey + " to the top");
                eventPublisher.publish(new GenericAnalyticsEvent("com.atlassianlabs.bamboo.plugins.queuepriority.notreordered"));
            }
        }
        else
        {
            final PlanResultKey latestQueuedJob = queuedPlans.get(queuedPlans.size() - 1);
            final BuildQueuePosition queuePosition = buildQueueManager.getQueuePosition(latestQueuedJob);
            final int newPosition = queuePosition.getQueuePosition() + 1;
            final boolean success
                    = buildQueueManager.reorderInQueue(queuedPlanResultKey, newPosition);
            if (success)
            {
                eventPublisher.publish(new ReorderedEvent(newPosition, position.getQueuePosition() + 1));
            }
            else
            {
                log.info("Couldn't reorder " + queuedPlanResultKey + " to index: " + newPosition);
                eventPublisher.publish(new GenericAnalyticsEvent("com.atlassianlabs.bamboo.plugins.queuepriority.notreordered"));
            }
        }
    }
}
