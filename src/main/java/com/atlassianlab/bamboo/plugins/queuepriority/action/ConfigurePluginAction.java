package com.atlassianlab.bamboo.plugins.queuepriority.action;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.configuration.GlobalAdminAction;
import com.atlassian.bandana.BandanaManager;
import com.atlassianlab.bamboo.plugins.queuepriority.PlanPriorityConfigurationPlugin;

public class ConfigurePluginAction extends GlobalAdminAction
{
    private BandanaManager bandanaManager;

    private boolean configByAdminOnly;
    private boolean success = false;

    public String input()
    {
        final Object isAvailableForAdmin = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, PlanPriorityConfigurationPlugin.BANDANA_KEY_ADMIN_ONLY);
        configByAdminOnly = isAvailableForAdmin == null ? false : Boolean.valueOf(isAvailableForAdmin.toString());
        return INPUT;
    }

    public String save()
    {
        bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, PlanPriorityConfigurationPlugin.BANDANA_KEY_ADMIN_ONLY, String.valueOf(configByAdminOnly));
        success = true;
        return SUCCESS;
    }

    public void setConfigByAdminOnly(boolean configByAdminOnly)
    {
        this.configByAdminOnly = configByAdminOnly;
    }

    public boolean isConfigByAdminOnly()
    {
        return configByAdminOnly;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }
}
