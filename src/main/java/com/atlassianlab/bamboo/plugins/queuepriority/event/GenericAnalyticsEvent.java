package com.atlassianlab.bamboo.plugins.queuepriority.event;

import com.atlassian.analytics.api.annotations.EventName;

public class GenericAnalyticsEvent
{
    private final String eventName;

    public GenericAnalyticsEvent(String eventName)
    {
        this.eventName = eventName;
    }

    @EventName
    public String getEventName()
    {
        return eventName;
    }
}
