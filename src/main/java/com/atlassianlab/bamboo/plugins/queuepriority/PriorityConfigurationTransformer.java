package com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plugin.module.ext.CustomBuildDefinitionTransformer;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bandana.BandanaManager;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

import static com.atlassianlab.bamboo.plugins.queuepriority.PlanPriorityConfigurationPlugin.BUILD_PRIORITY_KEY;

public class PriorityConfigurationTransformer implements CustomBuildDefinitionTransformer
{
    private BandanaManager bandanaManager;
    private BambooPermissionManager permissionManager;

    @Override
    public void transformBuildDefinition(@NotNull Map<String, Object> configObjects,
                                         @NotNull Map<String, String> configParameters,
                                         @NotNull BuildDefinition buildDefinition)
    {
        boolean isCanEdit = isCanEdit();
        if (isCanEdit)
        {
            configObjects.put(BUILD_PRIORITY_KEY, configParameters.getOrDefault(BUILD_PRIORITY_KEY, Priority.DEFAULT.name()));
        }
    }

    private boolean isCanEdit()
    {
        if (bandanaManager == null || permissionManager == null)
        {
            return false;
        }
        Object isEditedByAdminOnly = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, PlanPriorityConfigurationPlugin.BANDANA_KEY_ADMIN_ONLY);
        return isEditedByAdminOnly == null || !Boolean.valueOf(isEditedByAdminOnly.toString()) || permissionManager.hasGlobalPermission(BambooPermission.ADMINISTRATION);
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    public void setPermissionManager(BambooPermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }
}
