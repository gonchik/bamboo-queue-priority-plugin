package com.atlassianlab.bamboo.plugins.queuepriority;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanHelper;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.v2.build.BaseConfigurablePlugin;
import com.atlassian.bamboo.v2.build.configuration.MiscellaneousBuildConfigurationPlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.sal.api.message.I18nResolver;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PlanPriorityConfigurationPlugin extends BaseConfigurablePlugin implements MiscellaneousBuildConfigurationPlugin
{
    public static final String BANDANA_KEY_ADMIN_ONLY = "com.atlassianlab.bamboo.plugins.queuepriority:OnlyAdminCanSetPlanPriority";

    static final String BUILD_PRIORITY_KEY = "custom.queue.priority";
    private static final String PRIORITY_VALUES = "planPriorityValues";
    private static final String PRIORITY_VALUE = "planPriorityValue";
    private static final String CAN_MODIFY_PRIORITY = "isPlanPriorityValueEditingDisabled";

    private I18nResolver i18nResolver;
    private BandanaManager bandanaManager;
    private BambooPermissionManager permissionManager;

    @Override
    protected void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final Plan plan)
    {
        super.populateContextForView(context, plan);

        context.put("planPriorityConfig",
                PlanHelper.getConfigObject(plan, BUILD_PRIORITY_KEY, String.class));
    }

    @Override
    protected void populateContextForEdit(@NotNull Map<String, Object> context,
                                          @NotNull BuildConfiguration buildConfiguration,
                                          @Nullable Plan plan)
    {
        super.populateContextForEdit(context, buildConfiguration, plan);
        if (plan == null)
        {
            return;
        }
        final List<Pair> values = Arrays.asList(
                Pair.make(Priority.URGENT.name(), i18nResolver.getText("priority.urgent")),
                Pair.make(Priority.IMPORTANT.name(), i18nResolver.getText("priority.important")),
                Pair.make(Priority.DEFAULT.name(), i18nResolver.getText("priority.default")),
                Pair.make(Priority.IDLE.name(), i18nResolver.getText("priority.idle"))
        );

        context.put(PRIORITY_VALUES, values);
        context.put(PRIORITY_VALUE, PlanHelper.getConfigObject(plan, BUILD_PRIORITY_KEY, String.class));
        context.put(CAN_MODIFY_PRIORITY, isEditingDisabled());
    }

    private boolean isEditingDisabled()
    {
        final Object isAdminOnly = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_ADMIN_ONLY);
        final boolean isAvailableForAdminOnly = isAdminOnly != null && Boolean.valueOf(isAdminOnly.toString());
        boolean isEditingDisabled = false;
        if (isAvailableForAdminOnly)
        {
            isEditingDisabled = !permissionManager.hasGlobalPermission(BambooPermission.ADMINISTRATION);
        }
        return isEditingDisabled;
    }

    @Override
    public boolean isApplicableTo(@NotNull Plan plan)
    {
        return true;
    }

    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    public void setPermissionManager(BambooPermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }
}
