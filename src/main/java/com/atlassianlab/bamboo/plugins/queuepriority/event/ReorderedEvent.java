package com.atlassianlab.bamboo.plugins.queuepriority.event;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("com.atlassianlabs.bamboo.plugins.queuepriority.reordered")
public class ReorderedEvent
{
    private final int queueSize;
    private final int newPosition;

    public ReorderedEvent(int newPosition, int queueSize)
    {
        this.queueSize = queueSize;
        this.newPosition = newPosition;
    }

    public int getQueueSize()
    {
        return queueSize;
    }

    public int getNewPosition()
    {
        return newPosition;
    }
}
