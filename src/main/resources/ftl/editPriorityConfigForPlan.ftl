[#if isPlanPriorityValueEditingDisabled]
    [#assign whyEditIsDisabled][@s.text name='build.priority.edit.disabled' /][/#assign]
[/#if]

[@ui.bambooSection titleKey='build.priority.title']
    [@s.select name='custom.queue.priority' labelKey='build.priority' disabled=isPlanPriorityValueEditingDisabled
        list=planPriorityValues value=planPriorityValue
        helpIconCssClass='aui-iconfont-info'
        helpDialog=whyEditIsDisabled
        listKey='first' listValue='second']
    [/@s.select]
[/@ui.bambooSection]