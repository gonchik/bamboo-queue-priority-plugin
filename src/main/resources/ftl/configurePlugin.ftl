[#-- @ftlvariable name="action" type="com.atlassianlab.bamboo.plugins.queuepriority.action.ConfigurePluginAction" --]
[#-- @ftlvariable name="" type="com.atlassianlab.bamboo.plugins.queuepriority.action.ConfigurePluginAction" --]

<html>
<head>
    <title>[@ww.text name='build.priority.admin.title' /]</title>
    <meta name="decorator" content="adminpage">
    [#if success]
        <script type="application/javascript">
            require(['aui/flag'], function(Flag) {
                new Flag({
                    type: 'success',
                    close: 'auto',
                    body: '${action.getText("build.priority.admin.updated")}'
                });
            });
        </script>
    [/#if]
</head>
<body>

[@ww.form action='savePluginConfiguration' namespace='/admin/plugins/bamboo-queue-priority-plugin'
    titleKey="build.priority.admin.title" submitLabelKey='global.buttons.update']

    [@ui.bambooSection  titleKey='build.priority.admin.visibility.title' headerWeight='h3']
        [@s.checkbox labelKey='build.priority.admin.visibility.text' name='configByAdminOnly'/]
    [/@ui.bambooSection]
[/@ww.form]
</body>

</html>